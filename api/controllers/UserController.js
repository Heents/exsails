/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	'add': function (req, res) {
        res.view();
    },

    create: function (req, res, next) {
        User.create( req.params.all(), function userCreated (err, user) {
            if (err) {
                console.log(err);
                req.session.flash = {
                    err: err
                }
                return res.redirect('/user/add');
            }

            res.redirect('/user/'+user.id);
        });
    },

    show: function(req, res, next) {
        User.findOne(req.param('id'), function foundUser(err, user){
            if (err) return next(err);
            if (!user) return next('L\'usuari no existeix');
            res.view({
                user:user
            });
        });
    },

    index: function(req, res, next) {
        User.find(function foundUser(err,users){
            if (err) return next(err);

            res.view({
                users:users
            });
        });
    },

    edit: function(req,res, next){
        User.findOne(req.param('id'),function foundUser(err,user){
            if (err) return next(err);
            if (!user) return next('L\'usuari no existeix');
            res.view({
                user:user
            });
        });
    },

    update: function(req, res, next) {
        User.update(req.param('id'), req.params.all(), function userUpdated(err){
            if (err) {
                console.log(err);
                req.session.flash = {
                    err: err
                }
                return res.redirect('/user/edit/'+req.param('id'));
            }
            res.redirect('/user/'+req.param('id')+'');
        });
    },

    remove: function(req,res,next){
        User.findOne(req.param('id'),function foundUser(err,user){
            if (err) return next(err);
            if (!user) return next('L\'usuari no existeix');

            User.destroy(req.param('id'), function userDestroyed(err){
                if(err) return next(err);
            });

            res.redirect('/user');
        })
    }


};